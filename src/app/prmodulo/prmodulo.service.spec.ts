import { TestBed } from '@angular/core/testing';

import { PrmoduloService } from './prmodulo.service';

describe('PrmoduloService', () => {
  let service: PrmoduloService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrmoduloService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
