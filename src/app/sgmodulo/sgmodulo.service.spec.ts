import { TestBed } from '@angular/core/testing';

import { SgmoduloService } from './sgmodulo.service';

describe('SgmoduloService', () => {
  let service: SgmoduloService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SgmoduloService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
