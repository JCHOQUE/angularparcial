import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SgmoduloComponent } from './sgmodulo.component';

describe('SgmoduloComponent', () => {
  let component: SgmoduloComponent;
  let fixture: ComponentFixture<SgmoduloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SgmoduloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SgmoduloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
